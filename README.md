homebrew-brew-fp
================

## Installation from GitLab

```sh
brew tap falconepl/brew-fp git@gitlab.com:falconepl/homebrew-brew-fp.git
brew install brew-fp
```

## Local installation

```sh
brew install --build-from-source --formula brew-fp.rb
```
