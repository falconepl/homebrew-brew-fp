class BrewFp < Formula
    desc "Homebrew formula playground for testing purposes"
    homepage "https://gitlab.com/falconepl/brew-formula-playground"
    url "https://gitlab.com/falconepl/brew-formula-playground/-/raw/master/brew-fp.tar.gz"
    sha256 "e61610ebe0614755c1026bcad361c1026e96296d0ca49fdea9ce671d150ddf60"
    version "0.0.1"

    def install
      bin.install "brew-fp"
    end
  end
